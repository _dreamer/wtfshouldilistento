import configparser

class Settings():
    def __init__(self):
        self.__config = configparser.ConfigParser()
        self.__config.read('config.ini')

    @property
    def api_key(self):
     return self.__config['credentials']['apiKey']

    @property
    def secret(self):
        return self.__config['credentials']['secret']

