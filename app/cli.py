from config import Settings
import requests
import random
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-A', '--artist', type=str, required=True,
        help='Enter artist name to find similar music.'
    )

    args = parser.parse_args()
    artists = api_call(args)
    artist = random_artist(artists)

    print(artist)

def api_call(args):
    base_url = 'http://ws.audioscrobbler.com/2.0/'
    params = {
        'method' : 'artist.getSimilar',
        'artist' : args.artist,
        'autocorrect' : 1,
        'format' : 'json',
        'api_key' : Settings().api_key
    }

    r = requests.get(base_url, params=params)
    
    if r.status_code == 200:
        return r.json()['similarartists']['artist']

def random_artist(artists):
    artist_num = random.randrange(len(artists))
    return artists[artist_num]['name']

if __name__ == "__main__":
    main()